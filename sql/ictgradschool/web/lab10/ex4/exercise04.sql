DROP TABLE IF EXISTS dbtest_articles;

CREATE TABLE IF NOT EXISTS dbtest_articles (
  id     INT,
  title   VARCHAR(70),
  content TEXT,
  PRIMARY KEY (id)
);

INSERT INTO dbtest_articles VALUES  (1, 'Some Article', 'asdasd asjdad asdjdsa sdajda')