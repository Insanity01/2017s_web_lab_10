-- Answers to Exercise 2 here
DROP TABLE IF EXISTS dbtest_user;

CREATE TABLE IF NOT EXISTS dbtest_user (
  username  VARCHAR(15),
  firstname VARCHAR(55),
  lastname  VARCHAR(65),
  email     VARCHAR(30),
  PRIMARY KEY (username)

);

INSERT INTO dbtest_user VALUES ('programmer2', 'Bill', 'Peterson', 'bilL@microsoft.com');
INSERT INTO dbtest_user VALUES ('programmer3', 'Pete', 'Gates', 'bilL@microsoft.com');
INSERT INTO dbtest_user VALUES ('programmer4', 'Pete', 'Jones', 'bilL@microsoft.com');

SELECT *
FROM dbtest_user