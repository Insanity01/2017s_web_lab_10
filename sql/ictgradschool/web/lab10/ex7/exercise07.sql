DROP TABLE IF EXISTS dbtest_articles;
DROP TABLE IF EXISTS dbtest_comments;

CREATE TABLE IF NOT EXISTS dbtest_articles (
  id      INT,
  title   VARCHAR(70),
  content TEXT,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS dbtest_comments (
  commentid VARCHAR(20),
  foreignid INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (commentid),
  FOREIGN KEY (foreignid) REFERENCES dbtest_articles (id)
);

