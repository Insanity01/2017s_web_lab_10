-- Answers to Exercise 8 here
DELETE FROM dbtest_user
WHERE username = 'programmer1';

ALTER TABLE dbtest_video
  DROP COLUMN gender;

UPDATE dbtest_user
SET firstname = 'Alfred Schmidt', email = 'Frankfurt@gmail.com'
WHERE username = 'programmer2';

UPDATE dbtest_video
SET year_born = '1900'
WHERE name = 'Andrew Niccol';

UPDATE dbtest_store
SET barcode = '123456'
WHERE barcode = '01';