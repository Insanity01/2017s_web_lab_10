-- Answers to Exercise 9 here
SELECT * FROM dbtest_store;

SELECT barcode, title, director, name FROM dbtest_store;

INSERT INTO dbtest_articles VALUES (1, 'Vincent 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley ');
INSERT INTO dbtest_articles VALUES (2, 'Vincent 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley ');
INSERT INTO dbtest_articles VALUES (3, 'Vincent 3', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley ');
INSERT INTO dbtest_articles VALUES (4, 'Vincent 4', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley ');

SELECT title FROM dbtest_articles;

SELECT DISTINCT director FROM dbtest_store;

SELECT title FROM dbtest_store WHERE weeklyrate <=2;

SELECT name FROM dbtest_store ORDER BY name;

SELECT firstname FROM dbtest_user WHERE firstname like 'pete%';

SELECT firstname, lastname  FROM dbtest_user WHERE firstname like 'pete%' OR lastname like 'pete%';

